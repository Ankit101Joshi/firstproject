// function sum(a, b) {
//   return a + b;
// }

// module.exports = sum;

// function sumReduce(arr) {
//   return console.log(arr.reduce((acc, curr) => acc + curr, 0));
// }

// module.exports = sumReduce;

// function myFunction(input) {
//   if (typeof input !== "number") {
//     throw new Error("Invalid Input ");
//   }
// }

// module.exports = myFunction;

// function fetchData(callback) {
//   setTimeout(() => {
//     callback("Peanut Butter");
//   }, 1000);
// }

// module.exports = fetchData;

// function fetchPromise() {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => resolve("Peanut Butter"), 1000);
//   });
// }

// module.exports = fetchPromise;

// function walkTheDog() {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       const dogWalked = true; // Corrected typo here
//       if (dogWalked) {
//         resolve("The Dog has Been Walked");
//       } else {
//         reject("The Dog was Not Walked");
//       }
//     }, 1000);
//   });
// }

// async function doChores() {
//   try {
//     let walkDogResult = await walkTheDog();
//     console.log(walkDogResult);
//   } catch (error) {
//     console.log(error);
//   }
// }

// doChores();

async function fetchUserData() {
  const response = await axios.get("https://gorest.co.in/public-api/users");
  const data = await response.json();
  console.log(data);
}

module.exports = fetchUserData;
