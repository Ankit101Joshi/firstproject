const fetchUserData = require("./sum");
const axios = require("axios");

let get = (URI) => {
  return;
};

// test("Sum of [1,2,3] should be 6", () => {
//   expect(sumReduce([1, 2, 3])).toBe(6);
// });

// test("GET /users returns a list of users", async () => {
//   try {
//     const response = await axios.get("https://gorest.co.in/public-api/users");

//     // Check status code
//     expect(response.status).toEqual(200, "Status code should be 200");

//     // Check if data is an array
//     expect(Array.isArray(response.data.data)).toBeTruthy();

//     // Assuming the response has at least one user
//     const firstUser = response.data.data[0];
//     expect(response.data.data[0]).toHaveProperty("id");
//     expect(firstUser).toHaveProperty("name");
//     expect(firstUser).toHaveProperty("email");
//   } catch (error) {
//     // If there's an error with the request
//     throw new Error(`API request failed: ${error.message}`);
//   }
// });

const url = "https://gorest.co.in/public/v2/users/6752839";
const token =
  "Bearer 18f505d32440bd75ea0d9ebce6a28ba257594a80881443b1a4db9a80a0ab87e4";
const updatedUserData = {
  id: 6752839,
  name: "Deeptimoyee Chaturvedi",
  email: "chaturvedi_deeptimoyee@kilback-senger.test",
  gender: "female",
  status: "active",
};

const config = {
  header: {
    Authorization: token,
    "Content-Type": "application/json",
  },
};

test("PUT /users/:id updates a user", async () => {
  try {
    const response = await axios.put(url, updatedUserData, config);

    expect(response.data.data.id).toEqual(response.data.data.id);
    expect(response.data.data.name).toEqual(response.data.data.name);
    expect(response.data.data.email).toEqual(response.data.data.email);
    expect(response.data.data.gender).toEqual(response.data.data.gender);
    expect(response.data.data.status).toEqual(response.data.data.status);
  } catch (error) {}
});

test("Throws on invalid input", () => {
  expect(() => {
    myFunction(3);
  }).toThrow();
});

// test("The data is peanut butter", (done) => {
//   function callback(data) {
//     try {
//       expect(data).toBe("Peanut Butter");
//       done();
//     } catch (error) {
//       done(error);
//     }
//   }

//   fetchData(callback);
// });

// test("This is Promise Test", () => {
//   return expect(fetchPromise()).resolves.toBe("Peanut Butter");
// });

// test("The fetch fails with an error", () => {
//   return expect(fetchPromise()).rejects.toThrow("error");
// });

// test("The Data is async/Await", async () => {
//   const data = await fetchPromise();
//   expect(data).toBe("Peanut Butter");
// });

// test("Mock implementation of a basic funtion", () => {
//   const mock = jest.fn((x) => 42 + x);
//   expect(mock(1)).toBe(43);
// });

test("spying on a method of a object", () => {
  const video = {
    play() {
      return true;
    },
  };
  const spy = jest.spyOn(video, "play");
  video.play();

  expect(spy).toHaveBeenCalled();
  spy.mockRestore();
});

test("Fetch user data retrives all user data correctly", async () => {
  const userData = await fetchUserData();
});
