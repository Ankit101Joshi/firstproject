const puppeteer = require("puppeteer");

describe("End-to-End Test", () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
    await page.goto("https://www.mymottiv.com/");
  });

  test("Mottive Page loads", async () => {
    const pageTitle = await page.title();
    expect(pageTitle).toBe(
      "MOTTIV | Marathon, Running & Triathlon Training App"
    );
  });

  afterAll(async () => {
    await browser.close();
  });
});
