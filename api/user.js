const axios = require("axios");

async function fetchUserData() {
  try {
    const response = await axios.get("https://gorest.co.in/public-api/users");
    return response; // Just return the response, no need to await here
  } catch (error) {
    console.error("Error fetching user data:", error.message);
    throw error;
  }
}

async function fetchSingleUser(userId) {
  try {
    const response = await axios.get(
      `https://gorest.co.in/public/v2/users/${userId}`
    );
    return response; // Just return the response, no need to await here
  } catch (error) {
    console.error(`Error fetching user id ${userId}: ${error.message}`);
    throw error;
  }
}

// getUsersOverAge.js
function getUsersOverAge(users, minAge) {
  return users.filter((user) => user.age > minAge);
}

module.exports = { fetchUserData, fetchSingleUser, getUsersOverAge };
