const userData = {
  userId: 6761988,
  name: "Mr. Aanand Chattopadhyay",
  email: "aanand_mr_chattopadhyay@kuvalis-beier.test",
  gender: "female",
  status: "inactive",
};

module.exports = { userData };
