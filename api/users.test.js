const { fetchUserData, fetchSingleUser, getUsersOverAge } = require("./user");
const { userData } = require("./testData");

test("Fetch all the user", async () => {
  const response = await fetchUserData();
  expect(response.status).toEqual(200);
});

test("Fetch and Verify Single User Data", async () => {
  const { userId } = userData;

  let response;
  try {
    response = await fetchSingleUser(userId);
  } catch (error) {
    // Handle API call errors
    console.error("Error fetching user data:", error.message);
    throw error;
  }

  expect(response.status).toBe(200);
  expect(response.data.id).toBe(userId);

  // Compare each property of the user data
  Object.entries(userData).forEach(([key, value]) => {
    if (key !== "userId") {
      expect(response.data[key]).toBe(value);
    }
  });
});

const users = [
  { name: "Alice", age: 25 },
  { name: "Bob", age: 30 },
  { name: "Charlie", age: 20 },
];

test("getUsersOverAge filters users correctly", () => {
  expect(getUsersOverAge(users, 25)).toEqual([{ name: "Bob", age: 30 }]);
});

test("getUsersOverAge returns empty array if no users match", () => {
  expect(getUsersOverAge(users, 40)).toEqual([]);
});
